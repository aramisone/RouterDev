package com.example.plugintest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import com.demo.router.annotations.Destination

@Destination(url = "router://main",description = "主界面")
@Route(path = "Alirouter://main",name = "主界面")
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}