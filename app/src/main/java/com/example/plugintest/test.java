package com.example.plugintest;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.demo.router.annotations.Destination;

/**
 * author  : Liushuai
 * time    : 2022/5/10 14:45
 * desc    :
 */
@Destination(url = "router://main",description = "主界面")
@Route(path = "Alirouter://main",name = "测试")
class Test{

}