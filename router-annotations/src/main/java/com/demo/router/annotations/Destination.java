package com.demo.router.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * author  : Liushuai
 * time    : 2022/5/10 14:10
 * desc    :
 */

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.CLASS)
public @interface Destination {
    /**
     * 页面URL
     * @return
     */
    String url();

    /**
     * 页面描述
     * @return
     */
    String description();
}
