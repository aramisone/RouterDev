
package com.plugin.gradle

import groovy.json.JsonSlurper
import org.gradle.api.Plugin

import org.gradle.api.Project


class RouterPlugin implements  Plugin<Project> {
    //注入插件的逻辑
    @Override
    void apply(Project project) {
        println("RouterPlugin , apply from ${project.name}")

        //自动配置注解处理器中的路径参数
        if (project.extensions.findByName("kapt") != null) {
            project.extensions.findByName("kapt").arguments{
                arg("root_project_dir",project.rootProject.projectDir.absolutePath)
            }
        }

        //删除旧的路由文档
        project.clean.doFirst {
            println("RouterPlugin , clean")
            File mapFile = new File(project.rootProject.projectDir,"router_mapping")
            if (mapFile.exists()) {
                mapFile.deleteDir()
            }
        }

        project.getExtensions().create("router",RouterExtension)

        //配置阶段结束了 就可以获取配置的参数
        project.afterEvaluate {
            RouterExtension extension = project["router"]
            println("外部配置路径为sss：${extension.saveDir}")

            // javac任务之后生成汇总md文档
            project.tasks.findAll { task->
                task.name.startsWith('compile') && task.name.endsWith('JavaWithJavac')
            }.each { task->
                //任务最后做的事情
                println("生成md文档}")
                task.doLast {
                    File routerMappingDir =new File(project.rootProject.projectDir,"router_mapping")

                    if (!routerMappingDir.exists()) {
                        return
                    }

                    File[] allChildFiles = routerMappingDir.listFiles()

                    if (allChildFiles.size() < 1) {
                        return
                    }

                    StringBuilder stringBuilder = new StringBuilder()
                    stringBuilder.append("# 页面路由文档\n\n")
                    allChildFiles.each { child ->
                         if(child.name.endsWith(".json")){
                             //将json转换为List
                             JsonSlurper jsonSlurper = new JsonSlurper()
                             def content = jsonSlurper.parse(child)
                             content.each { itemContent->
                                 def url = itemContent['url']
                                 def description = itemContent['description']
                                 def realPath = itemContent['realPath']
                                 stringBuilder.append("## $description")
                                 stringBuilder.append(" -url :$url")
                                 stringBuilder.append(" - realPath: $realPath \n")
                             }
                         }
                     }
                    println("页面路由文档}")
                    File fileDir = new File(extension.saveDir,"页面路由文档.md")
                    if (fileDir.exists()) {
                        fileDir.delete()
                    }
                    fileDir.write(stringBuilder.toString())
                }
            }
        }
    }
}